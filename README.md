Release Notes
-------------

The Release Notes contain important information for new users and for
an update from the previous version of Debian. It is written in
reStructuredText, which allows the creation of various output files in the
formats HTML, text, PDF, ...

You can find more information about the Release Notes at
https://wiki.debian.org/ReleaseNotes
and
https://www.debian.org/doc/user-manuals#relnotes


Building the RN
---------------

To build it, you need these packages:
- latexmk
- python3-distro-info
- python3-sphinx
- python3-stemmer
- tex-gyre
- texinfo
- texlive-fonts-recommended
- texlive-lang-all
- texlive-latex-extra
- texlive-latex-recommended

See also the debian/control file.


Just call "make" to build it completely. To build the release notes only in a
single format (html, txt, pdf) for just one language (e.g. zh_CN, en, de),
call e.g.: "make html LANGS=de".

Contributing
------------

To submit new text to the Release Notes you can submit a merge request
against the repository at
https://salsa.debian.org/ddp-team/release-notes
or send a patch via the bts.


The release notes are written in reStructuredText, and here are some basic
guidelines and examples:
  **** To link to a website with more information:
	  Report bugs on our `bug tracking system <https://bugs.debian.org/>`__.
  **** To reference a man page on stable distribution (using manpages.debian.org):
	  See :url-man-stable:`yt-dlp.1`.
  **** To reference a man page on oldstable distribution (using manpages.debian.org):
	  See :url-man-oldstable:`slapd-hdb(5)`.
  **** To refer to a file or directory:
	  See the comments in ``/etc/apt/apt.conf.d/01autoremove``.
  **** To refer to a command that the user will run:
	  .. code-block:: console
	     # dpkg-reconfigure <GRUB_PACKAGE>
  **** To refer to a command as a noun:
	  Run ``update-grub`` to change this.
  **** To refer to the name of a Debian package:
	  The **webkit2gtk** source package is covered by security support.
  **** To refer to another documentation section in this file:
	  See also `Purging removed packages <#purge-removed-packages>`__.
       where <#purge-removed-packages> points to a section in the same file starting with
       .. _purge-removed-packages:
  **** To refer to a chapter in another file of this document:
	  We suggest that before upgrading you also read :ref:`ch-information`.
       where :ref:`ch-information` points to a chapter (in another .rst file) starting with
      .. _ch-information:
  **** For variables, or other things that don't quite fit into the above
       categories but need some markup:
	  ``GRUB_DISABLE_OS_PROBER=false``
  **** To mark text as important:
	  It is very important to do this **before** rebooting.
  **** To refer to specific Debian code names, use lower case:
	  *trixie*. 
       You can also use |OLDRELEASENAME| or |RELEASENAME| for text that is likely to
       remain in the release notes for several releases.
  **** But the word 'Debian' itself should always be with an upper case 'D' !
  **** For quotes, use "this" plain quote signs; avoid using “curved quotes” or »guillemets«.
          The plain quotes will be converted into the correct variant per language during build.
  **** To make a list of bullet points:
The `cloud team <https://wiki.debian.org/Teams/Cloud>`__ publishes Debian |RELEASENAME| for
several popular cloud computing services including:

-  OpenStack

-  Amazon Web Services

-  Microsoft Azure

  **** Generally the listitems are short with no full-stops at the end, but
       you can also put entire paragraphs inside each listitem:

The following archive areas, mentioned in the Social Contract and in the
Debian Policy, have been around for a long time:

-  main: the Debian distribution;

-  contrib: supplemental packages intended to work with the Debian
   distribution, but which require software outside of the distribution
   to either build or function;

-  non-free: supplemental packages intended to work with the Debian
   distribution that do not comply with the DFSG or have other problems
   that make their distribution problematic.

  **** To disable a paragraph in the output, but keep it in the document (and in
       the translations/po files), use the 'only' directive with the 'fixme' tag,
       followed by a blank line.
       The content of the chapter itself needs to be engaged:

.. only:: fixme

	No-longer-supported hardware
	~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	For a number of `arch`-based devices that were supported in
	|OLDRELEASENAME|, it is no longer viable for Debian to build the required
	``Linux`` kernel, due to hardware limitations. The unsupported devices
	are:

	-  foo

	-  bar


Translations
------------

Translators, please refer to the 'README.translators' document for
a full description of the translation process for this document.

Autobuilding of the RN
----------------------

The Release Notes are automatically built at www-master.debian.org in order to
publish them at https://www.debian.org/releases/<distribution>/releasenotes
and all the mirrors around the world.

An automatic task updates the www-master GIT copy to the latest revision
and builds the Release Notes for different releases.

Build logs are available at
   https://www-master.debian.org/build-logs/webwml/release-notes.log


The automatic task that runs the build is:
https://salsa.debian.org/webmaster-team/cron/blob/master/parts/7release-notes
which is called by the 'often' task at www-master:

```
# m h               dom mon dow command
 24 3,7,11,15,19,23 *   *   *   /srv/www.debian.org/cron/often
```

CI Builds
---------

The release-notes are built on each push on salsa.debian.org via a
gitlab pipeline.  The builds from the master branch will after a few
minutes be published on ddp-team.pages.debian.net.  Example links:

 * [English version](https://ddp-team.pages.debian.net/release-notes/amd64/release-notes/index.en.html)
 * [German version](https://ddp-team.pages.debian.net/release-notes/i386/release-notes/index.de.html)
 * [Translation statistics](https://ddp-team.pages.debian.net/release-notes/statistics.html)
