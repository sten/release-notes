.. _ch-about:

Introduction
========================

This document informs users of the Debian distribution about major
changes in version |RELEASE| (codenamed |RELEASENAME|).

The release notes provide information on how to upgrade safely from
release |OLDRELEASE| (codenamed |OLDRELEASENAME|) to the current release and
inform users of known potential issues they could encounter in that
process.

You can get the most recent version of this document from
|URL-R-N-STABLE|.

.. caution::

   Note that it is impossible to list every known issue and that
   therefore a selection has been made based on a combination of the
   expected prevalence and impact of issues.

Please note that we only support and document upgrading from the
previous release of Debian (in this case, the upgrade from
|OLDRELEASENAME|). If you need to upgrade from older releases, we suggest
you read previous editions of the release notes and upgrade to
|OLDRELEASENAME| first.

.. _bug-reports:

Reporting bugs on this document
--------------------------------------------------------------

We have attempted to test all the different upgrade steps described in
this document and to anticipate all the possible issues our users might
encounter.

Nevertheless, if you think you have found a bug (incorrect information
or information that is missing) in this documentation, please file a bug
in the `bug tracking system <https://bugs.debian.org/>`__ against the **release-notes**
package. You might first want to review the
`existing bug reports <https://bugs.debian.org/release-notes>`__
in case the issue you've found has already
been reported. Feel free to add additional information to existing bug
reports if you can contribute content for this document.

We appreciate, and encourage, reports providing patches to the
document's sources. You will find more information describing how to
obtain the sources of this document in `Sources for this
document <#sources>`__.

.. _upgrade-reports:

Contributing upgrade reports
--------------------------------------------------------

We welcome any information from users related to upgrades from
|OLDRELEASENAME| to |RELEASENAME|. If you are willing to share information
please file a bug in the `bug tracking system <https://bugs.debian.org/>`__ against the
**upgrade-reports** package with your results. We request that you compress
any attachments that are included (using ``gzip``).

Please include the following information when submitting your upgrade
report:

-  The status of your package database before and after the upgrade:
   **dpkg**'s status database available at ``/var/lib/dpkg/status`` and
   **apt**'s package state information, available at
   ``/var/lib/apt/extended_states``. You should have made a backup
   before the upgrade as described at :ref:`data-backup`, but you
   can also find backups of ``/var/lib/dpkg/status`` in
   ``/var/backups``.

-  Session logs created using ``script``, as described in
   :ref:`record-session`.

-  Your **apt** logs, available at ``/var/log/apt/term.log``, or your
   ``aptitude`` logs, available at ``/var/log/aptitude``.

.. note::

   You should take some time to review and remove any sensitive and/or
   confidential information from the logs before including them in a bug
   report as the information will be published in a public database.

.. _sources:

Sources for this document
--------------------------------------------------

The source of this document is in reStructuredText format, using the sphinx converter.
The HTML version is generated using *sphinx-build -b html*. The PDF
version is generated using *sphinx-build -b latex*. Sources for the Release
Notes are available in the Git repository of the *Debian Documentation
Project*. You can use the `web interface <https://salsa.debian.org/ddp-team/release-notes/>`__ to
access its files individually through the web and see their changes. For
more information on how to access Git please consult the `Debian
Documentation Project VCS information pages <https://www.debian.org/doc/vcs>`__.
