.. _ch-moreinfo:

More information on Debian
====================================================

.. _morereading:

Further reading
------------------------------

Beyond these release notes and the installation
guide (at |URL-INSTALLER-MANUAL|) further documentation on Debian is
available from the Debian Documentation Project (DDP), whose goal is to
create high-quality documentation for Debian users and developers, such
as the Debian Reference, Debian New Maintainers Guide, the Debian FAQ,
and many more. For full details of the existing resources see the
`Debian Documentation website <https://www.debian.org/doc/>`__ and the `Debian
Wiki <https://wiki.debian.org/>`__.

Documentation for individual packages is installed into
``/usr/share/doc/package``. This may include copyright information,
Debian specific details, and any upstream documentation.

.. _gethelp:

Getting help
------------------------

There are many sources of help, advice, and support for Debian users,
though these should only be considered after researching the issue in
available documentation. This section provides a short introduction to
these sources which may be helpful for new Debian users.

.. _lists:

Mailing lists
~~~~~~~~~~~~~~~~~~~~~~~~~~

The mailing lists of most interest to Debian users are the debian-user
list (English) and other debian-user-language lists (for other
languages). For information on these lists and details of how to
subscribe see `<https://lists.debian.org/>`__. Please check the
archives for answers to your question prior to posting and also adhere
to standard list etiquette.

.. _irc:

Internet Relay Chat
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Debian has an IRC channel dedicated to support and aid for Debian users,
located on the OFTC IRC network. To access the channel, point your
favorite IRC client at irc.debian.org and join ``#debian``.

Please follow the channel guidelines, respecting other users fully. The
guidelines are available at the `Debian Wiki <https://wiki.debian.org/DebianIRC>`__.

For more information on OFTC please visit the
`website <http://www.oftc.net/>`__.

.. _bugs:

Reporting bugs
----------------------------

We strive to make Debian a high-quality operating system; however that
does not mean that the packages we provide are totally free of bugs.
Consistent with Debian's "open development" philosophy and as a service
to our users, we provide all the information on reported bugs at our own
Bug Tracking System (BTS). The BTS can be browsed at `<https://bugs.debian.org/>`__.

If you find a bug in the distribution or in packaged software that is
part of it, please report it so that it can be properly fixed for future
releases. Reporting bugs requires a valid e-mail address. We ask for
this so that we can trace bugs and developers can get in contact with
submitters should additional information be needed.

You can submit a bug report using the program ``reportbug`` or manually
using e-mail. You can find out more about the Bug Tracking System and
how to use it by reading the reference documentation (available at
``/usr/share/doc/debian`` if you have **doc-debian** installed) or online at
the `Bug Tracking System <https://bugs.debian.org/>`__.

.. _contributing:

Contributing to Debian
--------------------------------------------

You do not need to be an expert to contribute to Debian. By assisting
users with problems on the various user support
`lists <https://lists.debian.org/>`__ you are contributing to the
community. Identifying (and also solving) problems related to the
development of the distribution by participating on the development
`lists <https://lists.debian.org/>`__ is also extremely helpful. To
maintain Debian's high-quality distribution, `submit bugs <https://bugs.debian.org/>`__
and help developers track them down and fix them. The tool
**how-can-i-help** helps you to find suitable reported bugs to work on. If
you have a way with words then you may want to contribute more actively
by helping to write `documentation <https://www.debian.org/doc/vcs>`__ or
`translating <https://www.debian.org/international/>`__ existing documentation into your own
language.

If you can dedicate more time, you could manage a piece of the Free
Software collection within Debian. Especially helpful is if people adopt
or maintain items that people have requested for inclusion within
Debian. The `Work Needing and Prospective Packages
database <https://www.debian.org/devel/wnpp/>`__ details this information. If you have an
interest in specific groups then you may find enjoyment in contributing
to some of Debian's `subprojects <https://www.debian.org/devel/#projects>`__ which
include ports to particular architectures and `Debian Pure
Blends <https://wiki.debian.org/DebianPureBlends>`__ for specific user groups, among many
others.

In any case, if you are working in the free software community in any
way, as a user, programmer, writer, or translator you are already
helping the free software effort. Contributing is rewarding and fun, and
as well as allowing you to meet new people it gives you that warm fuzzy
feeling inside.
