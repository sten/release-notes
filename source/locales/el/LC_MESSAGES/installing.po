# Copyright (C) 2020 Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
#
# galaxico <galas@tee.gr>, 2020, 2021, 2023.
msgid ""
msgstr ""
"Project-Id-Version:  \n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2023-10-06 00:00+0200\n"
"PO-Revision-Date: 2023-06-03 17:48+0300\n"
"Last-Translator: galaxico <galas@tee.gr>\n"
"Language: en_US\n"
"Language-Team: debian-l10n-greek@lists.debian.org\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.8.0\n"

#: ../installing.rst:4
msgid "Installation System"
msgstr "Σύστημα Εγκατάστασης"

# | msgid ""
# | "The Debian Installer is the official installation system for Debian.  It
# "
# | "offers a variety of installation methods.  Which methods are available to
# "
# | "install your system depends on your architecture."
#: ../installing.rst:6
#, fuzzy
msgid ""
"The Debian Installer is the official installation system for Debian. It "
"offers a variety of installation methods. The methods that are available "
"to install your system depend on its architecture."
msgstr ""
"Ο Εγκαταστάτης του Debian είναι το επίσημο σύστημα εγκατάστασης για το "
"Debian. Προσφέρει μια ποικιλία μεθόδων εγκατάστασης. Οι μέθοδοι που είναι"
" διαθέσιμες για εγκατάσταση στο σύστημά σας εξαρτάται από την "
"αρχιτεκτονική του."

#: ../installing.rst:10
#, fuzzy
msgid ""
"Images of the installer for |RELEASENAME| can be found together with the "
"Installation Guide on the Debian website (|URL-INSTALLER|)."
msgstr ""
"Εικόνες του εγκαταστάτη για την έκδοση |RELEASENAME| μπορούν να βρεθούν "
"μαζί με τον Οδηγό Εγκατάστασης στον σύνδεσμο |URL-INSTALLER|."

#: ../installing.rst:13
msgid ""
"The Installation Guide is also included on the first media of the "
"official Debian DVD (CD/blu-ray) sets, at:"
msgstr ""
"Ο Οδηγός Εγκατάστασης βρίσκεται επίσης στο πρώτο από τα μέσα του επίσημου"
" σετ Debian DVD (CD/blu-ray) στο:"

#: ../installing.rst:20
#, fuzzy
msgid ""
"You may also want to check the errata for debian-installer at |URL-"
"INSTALLER-ERRATA| for a list of known issues."
msgstr ""
"Πιθανόν να θέλετε να ελέγξετε επίσης τον σύνδεσμο |URL-"
"INSTALLER-ERRATA| του debian-installer για μια "
"λίστα γνωστών ζητημάτων."

#: ../installing.rst:26
msgid "What's new in the installation system?"
msgstr "Τι είναι καινούριο στο σύστημα εγκατάστασης;"

#: ../installing.rst:28
#, fuzzy
msgid ""
"There has been a lot of development on the Debian Installer since its "
"previous official release with Debian |OLDRELEASE|, resulting in improved"
" hardware support and some exciting new features or improvements."
msgstr ""
"Έχει γίνει αρκετή ανάπτυξη στον Εγκαταστάτη του Debian από την "
"προηγούμενη επίσημη έκδοσή του με το Debian |OLDRELEASE|, που έχει ως "
"αποτέλεσμα βελτιωμένη υποστήριξη υλικού και μερικά συναρπαστικά καινούρια"
" χαρακτηριστικά ή βελτιώσεις."

#: ../installing.rst:32
msgid ""
"If you are interested in an overview of the changes since "
"|OLDRELEASENAME|, please check the release announcements for the "
"|RELEASENAME| beta and RC releases available from the Debian Installer's "
"`news history <https://www.debian.org/devel/debian-installer/News/>`__."
msgstr ""
"Αν ενδιαφέρεστε για μια επισκόπηση των λεπτομερών αλλαγών από την "
"έκδοση |OLDRELEASENAME|, παρακαλούμε ελέγξτε τις ανακοινώσεις για την "
"κυκλοφορία της έκδοσης |RELEASENAME| τις εκδόσεις beta και RC "
"που είναι διαθέσιμες από τη σελίδα του Εγκαταστάτη του Debian "
"`news history <https://www.debian.org/devel/debian-installer/News/>`__."

#: ../installing.rst:41
msgid "Something"
msgstr "Κάτι"

#: ../installing.rst:43
msgid "Text"
msgstr "Κείμενο"

#: ../installing.rst:48
msgid "Cloud installations"
msgstr "Εγκαταστάσεις στο Νέφος"

# | msgid ""
# | "The <ulink url=\"&url-cloud-team;\">cloud team</ulink> publishes Debian "
# | "bullseye for several popular cloud computing services including:"
#: ../installing.rst:50
#, fuzzy
msgid ""
"The `cloud team <https://wiki.debian.org/Teams/Cloud>`__ publishes Debian"
" |RELEASENAME| for several popular cloud computing services including:"
msgstr ""
"Η `ομάδα Νέφους <https://wiki.debian.org/Teams/Cloud>`__ εκδίδει το Debian"
" |RELEASENAME| για αρκετές δημοφιλείς υπηρεσίες υπολογιστικού Νέφους "
"περιλαμβανομένων των:"

#: ../installing.rst:53
msgid "Amazon Web Services"
msgstr "Amazon Web Services"

#: ../installing.rst:55
msgid "Microsoft Azure"
msgstr "Microsoft Azure"

#: ../installing.rst:57
msgid "OpenStack"
msgstr "OpenStack"

#: ../installing.rst:59
msgid "Plain VM"
msgstr "Απλές εικονικές μηχανές (VM)"

# | msgid ""
# | "Cloud images provide automation hooks via cloud-init and prioritize fast
# "
# | "instance startup using specifically optimized kernel packages and grub "
# | "configurations.  Images supporting different architectures are provided "
# | "where appropriate and the cloud team endeavors to support all features "
# | "offered by the cloud service."
#: ../installing.rst:61
#, fuzzy
msgid ""
"Cloud images provide automation hooks via ``cloud-init`` and prioritize "
"fast instance startup using specifically optimized kernel packages and "
"grub configurations. Images supporting different architectures are "
"provided where appropriate and the cloud team endeavors to support all "
"features offered by the cloud service."
msgstr ""
"Οι εικόνες για Νέφος παρέχουν άγκιστρα (hooks) αυτοματοποίησης μέσω της "
"εντολής ``cloud-init`` και δίνουν προτεραιότητα σε ταχείες"
" εκκινήσεις συστημάτων χρησιμοποιώντας βελτιστοποιημένα πακέτα πυρήνα και"
" ρυθμίσεις του grub. Παρέχονται εικόνες που υποστηρίζουν διαφορετικές "
"αρχιτεκτονικές όπου είναι κατάλληλο και η ομάδα Νέφους προσπαθεί να "
"υποστηρίξει όλα τα χαρακτηριστικά που προσφέρονται από την υπηρεσία "
"Νέφους."

# | msgid ""
# | "The cloud team will provide updated images until the end of the LTS "
# | "period for bullseye.  New images are typically released for each point "
# | "release and after security fixes for critical packages.  The cloud team's
# "
# | "full support policy can be found <ulink url=\"&url-cloud-wiki-"
# | "imagelifecycle;\">here</ulink>."
#: ../installing.rst:67
#, fuzzy
msgid ""
"The cloud team will provide updated images until the end of the LTS "
"period for |RELEASENAME|. New images are typically released for each "
"point release and after security fixes for critical packages. The cloud "
"team's full support policy can be found `here "
"<https://wiki.debian.org/Cloud/ImageLifecycle>`__."
msgstr ""
"Η ομάδα Νέφους θα προσφέρει επικαιροποιημένες εικόνες μέχρι το τέλος της "
"περιόδου LTS για την έκδοση |RELEASENAME|. Καινούριες εικόνες κυκλοφορούν"
" τυπικά για κάθε σημειακή έκδοση και μετά από διορθώσεις ασφαλείας για "
"κρίσιμα πακέτα. Η πολιτική πλήρους υποστήριξης της ομάδας Νέφους μπορεί "
"να βρεθεί `εδώ "
"<https://wiki.debian.org/Cloud/ImageLifecycle>`__."

#: ../installing.rst:73
#, fuzzy
msgid ""
"More details are available at `<https://cloud.debian.org/>`__ and `on the"
" wiki <https://wiki.debian.org/Cloud/>`__."
msgstr ""
"Περισσότερες πληροφορίες είναι διαθέσιμες στον σύνδεσμο "
"`<https://cloud.debian.org/>`__ και στη σελίδα `στο wiki "
"<https://wiki.debian.org/Cloud/>`__."

#: ../installing.rst:79
msgid "Container and Virtual Machine images"
msgstr "Εικόνες για Container και Εικονικές Μηχανές"

#: ../installing.rst:81
msgid ""
"Multi-architecture Debian |RELEASENAME| container images are available on"
" `Docker Hub <https://hub.docker.com/_/debian>`__. In addition to the "
"standard images, a \"slim\" variant is available that reduces disk usage."
msgstr ""
"Εικόνες πολλαπλών αρχιτεκτονικών του Debian  |RELEASENAME| για container "
"διατίθενται στο `Docker Hub <https://hub.docker.com/_/debian>`__. "
"Επιπρόσθετα από τις συνήθεις εικόνες, διατίθεται επίσης μια εκδοχή "
"\"slim\" που μειώνει τη χρήση δίσκου."

#: ../installing.rst:86
#, fuzzy
msgid ""
"Virtual machine images for the Hashicorp Vagrant VM manager are published"
" to `Vagrant Cloud <https://app.vagrantup.com/debian>`__."
msgstr ""
"Εικόνες για εικονικές μηχανές (VM) για τον διαχειριστή Hashicorp Vagrant "
"είναι διαθέσιμες στον ιστότοπο `Vagrant Cloud <https://app.vagrantup.com/debian>`__."
