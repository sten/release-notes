.. _ch-upgrading:

Upgrades from Debian |OLDRELEASE| (|OLDRELEASENAME|)
==========================================================================================

.. _backup:

Preparing for the upgrade
--------------------------------------------------

We suggest that before upgrading you also read the information in
:ref:`ch-information`. That chapter covers potential issues which
are not directly related to the upgrade process but could still be
important to know about before you begin.

.. _data-backup:

Back up any data or configuration information
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Before upgrading your system, it is strongly recommended that you make a
full backup, or at least back up any data or configuration information
you can't afford to lose. The upgrade tools and process are quite
reliable, but a hardware failure in the middle of an upgrade could
result in a severely damaged system.

The main things you'll want to back up are the contents of ``/etc``,
``/var/lib/dpkg``, ``/var/lib/apt/extended_states`` and the output of:

.. code-block:: console

   $ dpkg --get-selections '*' # (the quotes are important)
         

If you use ``aptitude`` to manage packages on your system, you will also
want to back up ``/var/lib/aptitude/pkgstates``.

The upgrade process itself does not modify anything in the ``/home``
directory. However, some applications (e.g. parts of the Mozilla suite,
and the GNOME and KDE desktop environments) are known to overwrite
existing user settings with new defaults when a new version of the
application is first started by a user. As a precaution, you may want to
make a backup of the hidden files and directories ("dotfiles") in users'
home directories. This backup may help to restore or recreate the old
settings. You may also want to inform users about this.

Any package installation operation must be run with superuser
privileges, so either log in as ``root`` or use ``su`` or ``sudo`` to
gain the necessary access rights.

The upgrade has a few preconditions; you should check them before
actually executing the upgrade.

.. _inform-users:

Inform users in advance
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

It's wise to inform all users in advance of any upgrades you're
planning, although users accessing your system via an ``ssh`` connection
should notice little during the upgrade, and should be able to continue
working.

If you wish to take extra precautions, back up or unmount the ``/home``
partition before upgrading.

You will have to do a kernel upgrade when upgrading to |RELEASENAME|, so a
reboot will be necessary. Typically, this will be done after the upgrade
is finished.

.. _services-downtime:

Prepare for downtime on services
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

There might be services that are offered by the system which are
associated with packages that will be included in the upgrade. If this
is the case, please note that, during the upgrade, these services will
be stopped while their associated packages are being replaced and
configured. During this time, these services will not be available.

The precise downtime for these services will vary depending on the
number of packages being upgraded in the system, and it also includes
the time the system administrator spends answering any configuration
questions from package upgrades. Notice that if the upgrade process is
left unattended and the system requests input during the upgrade there
is a high possibility of services being unavailable [1]_ for a
significant period of time.

If the system being upgraded provides critical services for your users
or the network [2]_, you can reduce the downtime if you do a minimal
system upgrade, as described in `Minimal system
upgrade <#minimal-upgrade>`__, followed by a kernel upgrade and reboot,
and then upgrade the packages associated with your critical services.
Upgrade these packages prior to doing the full upgrade described in
`Upgrading the system <#upgrading-full>`__. This way you can ensure that
these critical services are running and available through the full
upgrade process, and their downtime is reduced.

.. _recovery:

Prepare for recovery
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Although Debian tries to ensure that your system stays bootable at all
times, there is always a chance that you may experience problems
rebooting your system after the upgrade. Known potential issues are
documented in this and the next chapters of these Release Notes.

For this reason it makes sense to ensure that you will be able to
recover if your system should fail to reboot or, for remotely managed
systems, fail to bring up networking.

If you are upgrading remotely via an ``ssh`` link it is recommended that
you take the necessary precautions to be able to access the server
through a remote serial terminal. There is a chance that, after
upgrading the kernel and rebooting, you will have to fix the system
configuration through a local console. Also, if the system is rebooted
accidentally in the middle of an upgrade there is a chance you will need
to recover using a local console.

For emergency recovery we generally recommend using the *rescue mode* of
the |RELEASENAME| Debian Installer. The advantage of using the installer
is that you can choose between its many methods to find one that best
suits your situation. For more information, please consult the section
"Recovering a Broken System" in chapter 8 of the Installation
Guide (at |URL-INSTALLER-MANUAL|) and the `Debian Installer
FAQ <https://wiki.debian.org/DebianInstaller/FAQ>`__.

If that fails, you will need an alternative way to boot your system so
you can access and repair it. One option is to use a special rescue or
`live install <https://www.debian.org/CD/live/>`__ image. After booting
from that, you should be able to mount your root file system and
``chroot`` into it to investigate and fix the problem.

.. _recovery-initrd:

Debug shell during boot using initrd
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The **initramfs-tools** package includes a debug shell [3]_ in the initrds
it generates. If for example the initrd is unable to mount your root
file system, you will be dropped into this debug shell which has basic
commands available to help trace the problem and possibly fix it.

Basic things to check are: presence of correct device files in ``/dev``;
what modules are loaded (``cat /proc/modules``); output of ``dmesg``
for errors loading
drivers. The output of ``dmesg`` will also show what device files have
been assigned to which disks; you should check that against the output
of ``echo $ROOT`` to make sure that the root file system is on the
expected device.

If you do manage to fix the problem, typing ``exit`` will quit the debug
shell and continue the boot process at the point it failed. Of course
you will also need to fix the underlying problem and regenerate the
initrd so the next boot won't fail again.

.. _recovery-shell-systemd:

Debug shell during boot using systemd
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If the boot fails under systemd, it is possible to obtain a debug root
shell by changing the kernel command line. If the basic boot succeeds,
but some services fail to start, it may be useful to add
``systemd.unit=rescue.target`` to the kernel parameters.

Otherwise, the kernel parameter ``systemd.unit=emergency.target`` will
provide you with a root shell at the earliest possible point. However,
this is done before mounting the root file system with read-write
permissions. You will have to do that manually with:

.. code-block:: console

   # mount -o remount,rw /
         

Another approach is to enable the systemd "early debug shell" via the
``debug-shell.service``. On the next boot this service opens a root
login shell on tty9 very early in the boot process. It can be enabled
with the kernel boot parameter ``systemd.debug-shell=1``, or made
persistent with ``systemctl enable debug-shell`` (in which case it
should be disabled again when debugging is completed).

More information on debugging a broken boot under systemd can be found
in the `Freedesktop.org Diagnosing Boot
Problems <https://freedesktop.org/wiki/Software/systemd/Debugging/>`__
article.

.. _upgrade-preparations:

Prepare a safe environment for the upgrade
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. important::

   If you are using some VPN services (such as **tinc**) consider that they
   might not be available throughout the upgrade process. Please see
   `Prepare for downtime on services <#services-downtime>`__.

In order to gain extra safety margin when upgrading remotely, we suggest
that you run upgrade processes in the virtual console provided by the
``screen`` program, which enables safe reconnection and ensures the
upgrade process is not interrupted even if the remote connection process
temporarily fails.

Users of the watchdog daemon provided by the **micro-evtd** package should
stop the daemon and disable the watchdog timer before the upgrade, to
avoid a spurious reboot in the middle of the upgrade process:

.. code-block:: console

   # service micro-evtd stop
   # /usr/sbin/microapl -a system_set_watchdog off
         

.. _system-status:

Start from "pure" Debian
------------------------------------------------

The upgrade process described in this chapter has been designed for
"pure" Debian stable systems. APT controls what is installed on your
system. If your APT configuration mentions additional sources besides
|OLDRELEASENAME|, or if you have installed packages from other releases or
from third parties, then to ensure a reliable upgrade process you may
wish to begin by removing these complicating factors.

The main configuration file that APT uses to decide what sources it
should download packages from is ``/etc/apt/sources.list``, but it can
also use files in the ``/etc/apt/sources.list.d/`` directory - for
details see
:url-man-stable:`sources.list(5)`.
If your system is using multiple source-list files then you will need to
ensure they stay consistent.

.. _upgrade-to-debian-oldrelease:

Upgrade to Debian |OLDRELEASE| (|OLDRELEASENAME|)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Only upgrades from Debian |OLDRELEASE| (|OLDRELEASENAME|) are supported.
Display your Debian version with:

.. code-block:: console

   $ cat /etc/debian_version
         

Please follow the instructions in the Release Notes for Debian
|OLDRELEASE| at |URL-R-N-OLDSTABLE|
to upgrade to Debian |OLDRELEASE| first if needed.

.. _upgrade-to-latest-point-release:

Upgrade to latest point release
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This procedure assumes your system has been updated to the latest point
release of |OLDRELEASENAME|. If you have not done this or are unsure,
follow the instructions in :ref:`old-upgrade`.

.. _debian-backports:

Debian Backports
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

`Debian Backports <https://backports.debian.org/>`__ allows users of Debian
stable to run more up-to-date versions of packages (with some tradeoffs
in testing and security support). The Debian Backports Team maintains a
subset of packages from the next Debian release, adjusted and recompiled
for usage on the current Debian stable release.

Packages from |OLDRELEASENAME|-backports have version numbers lower than
the version in |RELEASENAME|, so they should upgrade normally to
|RELEASENAME| in the same way as "pure" |OLDRELEASENAME| packages during the
distribution upgrade. While there are no known potential issues, the
upgrade paths from backports are less tested, and correspondingly incur
more risk.

.. caution::

   While regular Debian Backports are supported, there is no clean
   upgrade path from
   `sloppy <https://backports.debian.org/Instructions#index4h2>`__ backports
   (which use APT source-list entries referencing
   |OLDRELEASENAME|-backports-sloppy).

As with `Unofficial sources <#unofficial-sources>`__, users are advised
to remove |OLDRELEASENAME|-backports entries from their APT source-list
files before the upgrade. After it is completed, they may consider
adding |RELEASENAME|-backports (see
`<https://backports.debian.org/Instructions/>`__).

For more information, consult the `Backports Wiki
page <https://wiki.debian.org/Backports>`__.

.. _prepare-package-database:

Prepare the package database
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You should make sure the package database is ready before proceeding
with the upgrade. If you are a user of another package manager like
**aptitude** or **synaptic**, review any pending actions. A package scheduled
for installation or removal might interfere with the upgrade procedure.
Note that correcting this is only possible if your APT source-list files
still point to |OLDRELEASENAME| and not to stable or |RELEASENAME|;
see :ref:`old-sources`.

Remove obsolete packages
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

It is a good idea to `remove obsolete packages <#obsolete>`__ from your
system before upgrading. They may introduce complications during the
upgrade process, and can present security risks as they are no longer
maintained.

.. _removing-non-debian-packages:

Remove non-Debian packages
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Below there are two methods for finding installed packages that did not
come from Debian, using either ``apt`` or ``apt-forktracer``. Please
note that neither of them are 100% accurate (e.g. the apt example will
list packages that were once provided by Debian but no longer are, such
as old kernel packages).

.. code-block:: console

   $ apt list '?narrow(?installed, ?not(?origin(Debian)))'
   $ apt-forktracer | sort
       

Clean up leftover configuration files
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A previous upgrade may have left unused copies of configuration files;
`old versions <#configuration-changes>`__ of configuration files,
versions supplied by the package maintainers, etc. Removing leftover
files from previous upgrades can avoid confusion. Find such leftover
files with:

.. code-block:: console

   # find /etc -name '*.dpkg-*' -o -name '*.ucf-*' -o -name '*.merge-error'
       

.. _non-free-firmware:

The non-free and non-free-firmware components
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you have non-free firmware installed it is recommended to add
``non-free-firmware`` to your APT sources-list. For details see
:ref:`archive-areas` and :ref:`non-free-split`.

.. _proposed-updates:

The proposed-updates section
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you have listed the ``proposed-updates`` section in your APT
source-list files, you should remove it before attempting to upgrade
your system. This is a precaution to reduce the likelihood of conflicts.

Unofficial sources
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you have any non-Debian packages on your system, you should be aware
that these may be removed during the upgrade because of conflicting
dependencies. If these packages were installed by adding an extra
package archive in your APT source-list files, you should check if that
archive also offers packages compiled for |RELEASENAME| and change the
source item accordingly at the same time as your source items for Debian
packages.

Some users may have *unofficial* backported "newer" versions of packages
that *are* in Debian installed on their |OLDRELEASENAME| system. Such
packages are most likely to cause problems during an upgrade as they may
result in file conflicts [4]_. `Possible issues during
upgrade <#trouble>`__ has some information on how to deal with file
conflicts if they should occur.

.. _disable-apt-pinning:

Disabling APT pinning
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you have configured APT to install certain packages from a
distribution other than stable (e.g. from testing), you may have to
change your APT pinning configuration (stored in
``/etc/apt/preferences`` and ``/etc/apt/preferences.d/``) to allow the
upgrade of packages to the versions in the new stable release. Further
information on APT pinning can be found in
:url-man-stable:`apt_preferences(5)`.

.. _install-gpgv:

Check gpgv is installed
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

APT needs **gpgv** version 2 or greater to verify the keys used to sign
releases of |RELEASENAME|. Since **gpgv1** technically satisfies the
dependency but is useful only in specialized circumstances, users may
wish to ensure the correct version is installed with:

.. code-block:: console

   # apt install gpgv
       

.. _package-status:

Check package status
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Regardless of the method used for upgrading, it is recommended that you
check the status of all packages first, and verify that all packages are
in an upgradable state. The following command will show any packages
which have a status of Half-Installed or Failed-Config, and those with
any error status.

.. code-block:: console

   $ dpkg --audit
       

You could also inspect the state of all packages on your system using
``aptitude`` or with commands such as

.. code-block:: console

   $ dpkg -l | pager
       

or

.. code-block:: console

   # dpkg --get-selections '*' > ~/curr-pkgs.txt
       

Alternatively you can also use ``apt``.

.. code-block:: console

   # apt list --installed > ~/curr-pkgs.txt
       

It is desirable to remove any holds before upgrading. If any package
that is essential for the upgrade is on hold, the upgrade will fail.

.. code-block:: console

   $ apt-mark showhold
       

If you changed and recompiled a package locally, and didn't rename it or
put an epoch in the version, you must put it on hold to prevent it from
being upgraded.

The "hold" package state for ``apt`` can be changed using:

.. code-block:: console

   # apt-mark hold package_name
       

Replace ``hold`` with ``unhold`` to unset the "hold" state.

If there is anything you need to fix, it is best to make sure your APT
source-list files still refer to |OLDRELEASENAME| as explained in
:ref:`old-sources`.

.. _upgrade-process:

Preparing APT source-list files
--------------------------------------------------------------

Before starting the upgrade you must reconfigure APT source-list files
(``/etc/apt/sources.list`` and files under ``/etc/apt/sources.list.d/``)
to add sources for |RELEASENAME| and typically to remove sources for |OLDRELEASENAME|.

APT will consider all packages that can be found via any configured
archive, and install the package with the highest version number, giving
priority to the first entry in the files. Thus, if you have multiple
mirror locations, list first the ones on local hard disks, then CD-ROMs,
and then remote mirrors.

A release can often be referred to both by its codename (e.g. |OLDRELEASENAME|,
|RELEASENAME|) and by its status name (i.e. ``oldstable``, ``stable``,
``testing``, ``unstable``). Referring to a release by its codename has
the advantage that you will never be surprised by a new release and for
this reason is the approach taken here. It does of course mean that you
will have to watch out for release announcements yourself. If you use
the status name instead, you will just see loads of updates for packages
available as soon as a release has happened.

Debian provides two announcement mailing lists to help you stay up to
date on relevant information related to Debian releases:

-  By `subscribing to the Debian announcement mailing
   list <https://lists.debian.org/debian-announce/>`__, you will receive
   a notification every time Debian makes a new release. Such as when
   "|RELEASENAME|" changes from e.g. "testing" to "stable".

-  By `subscribing to the Debian security announcement mailing
   list <https://lists.debian.org/debian-security-announce/>`__, you
   will receive a notification every time Debian publishes a security
   announcement.

.. _network:

Adding APT Internet sources
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

On new installations the default is for APT to be set up to use the
Debian APT CDN service, which should ensure that packages are
automatically downloaded from a server near you in network terms. As
this is a relatively new service, older installations may have
configuration that still points to one of the main Debian Internet
servers or one of the mirrors. If you haven't done so yet, it is
recommended to switch over to the use of the CDN service in your APT
configuration.

To make use of the CDN service, add a line like this to your APT source
configuration (assuming you are using ``main`` and ``contrib``):

.. parsed-literal::

   deb https://deb.debian.org/debian |RELEASENAME| main contrib

After adding your new sources, disable the previously existing "``deb``"
lines by placing a hash sign (``#``) in front of them.

However, if you get better results using a specific mirror that is close
to you in network terms, this option is still available.

Debian mirror addresses can be found at `<https://www.debian.org/distrib/ftplist>`__
(look at the "list of Debian mirrors" section).

For example, suppose your closest Debian mirror is ``http://mirrors.kernel.org``. If you inspect
that mirror with a web browser, you will notice that the main
directories are organized like this:

.. parsed-literal::

   |URL-DEBIAN-MIRROR-EG|/debian/dists/|RELEASENAME|/main/...
   |URL-DEBIAN-MIRROR-EG|/debian/dists/|RELEASENAME|/contrib/...
       

To configure APT to use a given mirror, add a line like this (again,
assuming you are using ``main`` and ``contrib``):

.. parsed-literal::

   deb |URL-DEBIAN-MIRROR-EG|/debian |RELEASENAME| main contrib

Note that the "``dists``" is added implicitly, and the arguments after
the release name are used to expand the path into multiple directories.

Again, after adding your new sources, disable the previously existing
archive entries.

.. _localmirror:

Adding APT sources for a local mirror
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Instead of using remote package mirrors, you may wish to modify the APT
source-list files to use a mirror on a local disk (possibly mounted over
NFS).

For example, your package mirror may be under ``/var/local/debian/``,
and have main directories like this:

.. parsed-literal::

   /var/local/debian/dists/|RELEASENAME|/main/...
   /var/local/debian/dists/|RELEASENAME|/contrib/...
       

To use this with **apt**, add this line to your ``sources.list`` file:

.. parsed-literal::

   deb file:/var/local/debian |RELEASENAME| main contrib

Note that the “``dists``” is added implicitly, and the arguments after
the release name are used to expand the path into multiple directories.

After adding your new sources, disable the previously existing archive
entries in the APT source-list files by placing a hash sign (``#``) in
front of them.

.. _cdroms:

Adding APT sources from optical media
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you want to use *only* DVDs (or CDs or Blu-ray Discs), comment out
the existing entries in all the APT source-list files by placing a hash
sign (``#``) in front of them.

Make sure there is a line in ``/etc/fstab`` that enables mounting your
CD-ROM drive at the ``/media/cdrom`` mount point. For example, if
``/dev/sr0`` is your CD-ROM drive, ``/etc/fstab`` should contain a line
like:

.. code-block:: console

   /dev/sr0 /media/cdrom auto noauto,ro 0 0
       

Note that there must be *no spaces* between the words ``noauto,ro`` in
the fourth field.

To verify it works, insert a CD and try running

.. code-block:: console

   # mount /media/cdrom    # this will mount the CD to the mount point
   # ls -alF /media/cdrom  # this should show the CD's root directory
   # umount /media/cdrom   # this will unmount the CD
       

Next, run:

.. code-block:: console

   # apt-cdrom add
       

for each Debian Binary CD-ROM you have, to add the data about each CD to
APT's database.

.. _upgrading-packages:

Upgrading packages
------------------------------------

The recommended way to upgrade from previous Debian releases is to use
the package management tool ``apt``.

.. note::

   ``apt`` is meant for interactive use, and should not be used in
   scripts. In scripts one should use ``apt-get``, which has a stable
   output better suitable for parsing.

Don't forget to mount all needed partitions (notably the root and
``/usr`` partitions) read-write, with a command like:

.. code-block:: console

   # mount -o remount,rw /mountpoint
     

Next you should double-check that the APT source entries (in
``/etc/apt/sources.list`` and files under ``/etc/apt/sources.list.d/``)
refer either to "|RELEASENAME|" or to "stable". There should not be any
sources entries pointing to |OLDRELEASENAME|.

.. note::

   Source lines for a CD-ROM might sometimes refer to "``unstable``";
   although this may be confusing, you should *not* change it.

.. _record-session:

Recording the session
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

It is strongly recommended that you use the ``/usr/bin/script`` program
to record a transcript of the upgrade session. Then if a problem occurs,
you will have a log of what happened, and if needed, can provide exact
information in a bug report. To start the recording, type:

.. parsed-literal::

   # script -t 2>~/upgrade-|RELEASENAME|\-step.time -a ~/upgrade-|RELEASENAME|\-step.script
       

or similar. If you have to rerun the typescript (e.g. if you have to
reboot the system) use different step values to indicate which step of
the upgrade you are logging. Do not put the typescript file in a
temporary directory such as ``/tmp`` or ``/var/tmp`` (files in those
directories may be deleted during the upgrade or during any restart).

The typescript will also allow you to review information that has
scrolled off-screen. If you are at the system's console, just switch to
VT2 (using Alt+F2) and, after logging in, use

.. parsed-literal::

   # less -R ~root/upgrade-|RELEASENAME|.script


to view the file.

After you have completed the upgrade, you can stop ``script`` by typing
``exit`` at the prompt.

``apt`` will also log the changed package states in
``/var/log/apt/history.log`` and the terminal output in
``/var/log/apt/term.log``. ``dpkg`` will, in addition, log all package
state changes in ``/var/log/dpkg.log``. If you use ``aptitude``, it will
also log state changes in ``/var/log/aptitude``.

If you have used the *-t* switch for ``script`` you can use the
``scriptreplay`` program to replay the whole session:

.. parsed-literal::

   # scriptreplay ~/upgrade-|RELEASENAME|\-step.time ~/upgrade-|RELEASENAME|\-step.script
       

.. _updating-lists:

Updating the package list
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

First the list of available packages for the new release needs to be
fetched. This is done by executing:

.. code-block:: console

   # apt update
       

.. note::

   Users of apt-secure may find issues when using ``aptitude`` or
   ``apt-get``. For apt-get, you can use
   ``apt-get update --allow-releaseinfo-change``.

.. _sufficient-space:

Make sure you have sufficient space for the upgrade
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You have to make sure before upgrading your system that you will have
sufficient hard disk space when you start the full system upgrade
described in `Upgrading the system <#upgrading-full>`__. First, any
package needed for installation that is fetched from the network is
stored in ``/var/cache/apt/archives`` (and the ``partial/``
subdirectory, during download), so you must make sure you have enough
space on the file system partition that holds ``/var/`` to temporarily
download the packages that will be installed in your system. After the
download, you will probably need more space in other file system
partitions in order to both install upgraded packages (which might
contain bigger binaries or more data) and new packages that will be
pulled in for the upgrade. If your system does not have sufficient space
you might end up with an incomplete upgrade that is difficult to recover
from.

``apt`` can show you detailed information about the disk space needed
for the installation. Before executing the upgrade, you can see this
estimate by running:

.. parsed-literal::

   # apt -o APT::Get::Trivial-Only=true full-upgrade
   [ ... ]
   XXX upgraded, XXX newly installed, XXX to remove and XXX not upgraded.
   Need to get xx.xMB of archives.
   After this operation, AAAMB of additional disk space will be used.
       

.. note::

   Running this command at the beginning of the upgrade process may give
   an error, for the reasons described in the next sections. In that
   case you will need to wait until you've done the minimal system
   upgrade as in `Minimal system upgrade <#minimal-upgrade>`__ before
   running this command to estimate the disk space.

If you do not have enough space for the upgrade, ``apt`` will warn you
with a message like this:

.. parsed-literal::

   E: You don't have enough free space in /var/cache/apt/archives/.
       

In this situation, make sure you free up space beforehand. You can:

-  Remove packages that have been previously downloaded for installation
   (at ``/var/cache/apt/archives``). Cleaning up the package cache by
   running ``apt clean`` will remove all previously downloaded package
   files.

-  Remove forgotten packages. If you have used ``aptitude`` or ``apt``
   to manually install packages in |OLDRELEASENAME| it will have kept
   track of those packages you manually installed, and will be able to
   mark as redundant those packages pulled in by dependencies alone
   which are no longer needed due to a package being removed. They will
   not mark for removal packages that you manually installed. To remove
   automatically installed packages that are no longer used, run:

   .. code-block:: console

      # apt autoremove
              

   You can also use ``deborphan``, ``debfoster``, or ``cruft`` to find
   redundant packages. Do not blindly remove the packages these tools
   present, especially if you are using aggressive non-default options
   that are prone to false positives. It is highly recommended that you
   manually review the packages suggested for removal (i.e. their
   contents, sizes, and descriptions) before you remove them.

-  Remove packages that take up too much space and are not currently
   needed (you can always reinstall them after the upgrade). If you have
   **popularity-contest** installed, you can use ``popcon-largest-unused``
   to list the packages you do not use that occupy the most space. You
   can find the packages that just take up the most disk space with
   ``dpigs`` (available in the **debian-goodies** package) or with ``wajig``
   (running ``wajig size``). They can also be found with **aptitude**. Start
   ``aptitude`` in full-terminal mode, select Views > New Flat Package
   List, press l and enter ``~i``, then press S and enter
   ``~installsize``. This will give you a handy list to work with.

-  Remove translations and localization files from the system if they
   are not needed. You can install the **localepurge** package and configure
   it so that only a few selected locales are kept in the system. This
   will reduce the disk space consumed at ``/usr/share/locale``.

-  Temporarily move to another system, or permanently remove, system
   logs residing under ``/var/log/``.

-  Use a temporary ``/var/cache/apt/archives``: You can use a temporary
   cache directory from another filesystem (USB storage device,
   temporary hard disk, filesystem already in use, ...).

   .. note::

      Do not use an NFS mount as the network connection could be
      interrupted during the upgrade.

   For example, if you have a USB drive mounted on ``/media/usbkey``:

   1. remove the packages that have been previously downloaded for
      installation:

      .. code-block:: console

         # apt clean
                         

   2. copy the directory ``/var/cache/apt/archives`` to the USB drive:

      .. code-block:: console

         # cp -ax /var/cache/apt/archives /media/usbkey/
                         

   3. mount the temporary cache directory on the current one:

      .. code-block:: console

         # mount --bind /media/usbkey/archives /var/cache/apt/archives
                         

   4. after the upgrade, restore the original
      ``/var/cache/apt/archives`` directory:

      .. code-block:: console

         # umount /var/cache/apt/archives
                         

   5. remove the remaining ``/media/usbkey/archives``.

   You can create the temporary cache directory on whatever filesystem
   is mounted on your system.

-  Do a minimal upgrade of the system (see `Minimal system
   upgrade <#minimal-upgrade>`__) or partial upgrades of the system
   followed by a full upgrade. This will make it possible to upgrade the
   system partially, and allow you to clean the package cache before the
   full upgrade.

Note that in order to safely remove packages, it is advisable to switch
your APT source-list files back to |OLDRELEASENAME| as described in
:ref:`old-sources`.

.. _stop-monitoring-systems:

Stop monitoring systems
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

As ``apt`` may need to temporarily stop services running on your
computer, it's probably a good idea to stop monitoring services that can
restart other terminated services during the upgrade. In Debian, **monit**
is an example of such a service.

.. _minimal-upgrade:

Minimal system upgrade
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In some cases, doing the full upgrade (as described below) directly
might remove large numbers of packages that you will want to keep. We
therefore recommend a two-part upgrade process: first a minimal upgrade
to overcome these conflicts, then a full upgrade as described in
`Upgrading the system <#upgrading-full>`__.

To do this, first run:

.. code-block:: console

   # apt upgrade --without-new-pkgs
       

This has the effect of upgrading those packages which can be upgraded
without requiring any other packages to be removed or installed.

The minimal system upgrade can also be useful when the system is tight
on space and a full upgrade cannot be run due to space constraints.

If the **apt-listchanges** package is installed, it will (in its default
configuration) show important information about upgraded packages in a
pager after downloading the packages. Press q after reading to exit the
pager and continue the upgrade.

.. _upgrading-full:

Upgrading the system
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Once you have taken the previous steps, you are now ready to continue
with the main part of the upgrade. Execute:

.. code-block:: console

   # apt full-upgrade
       

This will perform a complete upgrade of the system, installing the
newest available versions of all packages, and resolving all possible
dependency changes between packages in different releases. If necessary,
it will install some new packages (usually new library versions, or
renamed packages), and remove any conflicting obsoleted packages.

When upgrading from a set of CDs/DVDs/BDs, you will probably be asked to
insert specific discs at several points during the upgrade. You might
have to insert the same disc multiple times; this is due to
inter-related packages that have been spread out over the discs.

New versions of currently installed packages that cannot be upgraded
without changing the install status of another package will be left at
their current version (displayed as "held back"). This can be resolved
by either using ``aptitude`` to choose these packages for installation
or by trying ``apt install package``.

.. _trouble:

Possible issues during upgrade
------------------------------------------------------------

The following sections describe known issues that might appear during an
upgrade to |RELEASENAME|.

.. _immediate-configure:

Full-upgrade fails with "Could not perform immediate configuration"
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In some cases the ``apt full-upgrade`` step can fail after downloading
packages with:

.. parsed-literal::

   E: Could not perform immediate configuration on 'package'.  Please see man 5 apt.conf under APT::Immediate-Configure for details.
         

If that happens, running
``apt full-upgrade -o APT::Immediate-Configure=0``
instead should allow the upgrade to proceed.

Another possible workaround for this problem is to temporarily add both
|OLDRELEASENAME| and |RELEASENAME| sources to your APT source-list files and
run ``apt update``.

Expected removals
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The upgrade process to |RELEASENAME| might ask for the removal of packages
on the system. The precise list of packages will vary depending on the
set of packages that you have installed. These release notes give
general advice on these removals, but if in doubt, it is recommended
that you examine the package removals proposed by each method before
proceeding. For more information about packages obsoleted in
|RELEASENAME|, see `Obsolete packages <#obsolete>`__.

.. _conflicts-loops:

Conflicts or Pre-Depends loops
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Sometimes it's necessary to enable the ``APT::Force-LoopBreak`` option
in APT to be able to temporarily remove an essential package due to a
Conflicts/Pre-Depends loop. ``apt`` will alert you of this and abort the
upgrade. You can work around this by specifying the option
``-o APT::Force-LoopBreak=1`` on the ``apt`` command line.

It is possible that a system's dependency structure can be so corrupt as
to require manual intervention. Usually this means using ``apt`` or

.. code-block:: console

   # dpkg --remove package_name
       

to eliminate some of the offending packages, or

.. code-block:: console

   # apt -f install
   # dpkg --configure --pending
       

In extreme cases you might have to force re-installation with a command
like

.. code-block:: console

   # dpkg --install /path/to/package_name.deb

.. _file-conflicts:

File conflicts
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

File conflicts should not occur if you upgrade from a "pure"
|OLDRELEASENAME| system, but can occur if you have unofficial backports
installed. A file conflict will result in an error like:

.. code-block:: console

   Unpacking <package-foo> (from <package-foo-file>) ...
   dpkg: error processing <package-foo> (--install):
   trying to overwrite `<some-file-name>',
   which is also in package <package-bar>
   dpkg-deb: subprocess paste killed by signal (Broken pipe)
   Errors were encountered while processing:
   <package-foo>

You can try to solve a file conflict by forcibly removing the package
mentioned on the *last* line of the error message:

.. code-block:: console

   # dpkg -r --force-depends package_name
       

After fixing things up, you should be able to resume the upgrade by
repeating the previously described ``apt`` commands.

.. _configuration-changes:

Configuration changes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

During the upgrade, you will be asked questions regarding the
configuration or re-configuration of several packages. When you are
asked if any file in the ``/etc/init.d`` directory, or the
``/etc/manpath.config`` file should be replaced by the package
maintainer's version, it's usually necessary to answer "yes" to ensure
system consistency. You can always revert to the old versions, since
they will be saved with a ``.dpkg-old`` extension.

If you're not sure what to do, write down the name of the package or
file and sort things out at a later time. You can search in the
typescript file to review the information that was on the screen during
the upgrade.

.. _console-change:

Change of session to console
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you are running the upgrade using the system's local console you
might find that at some points during the upgrade the console is shifted
over to a different view and you lose visibility of the upgrade process.
For example, this may happen in systems with a graphical interface when
the display manager is restarted.

To recover the console where the upgrade was running you will have to
use Ctrl+Alt+F1 (if in the graphical startup screen) or Alt+F1 (if in
the local text-mode console) to switch back to the virtual terminal 1.
Replace F1 with the function key with the same number as the virtual
terminal the upgrade was running in. You can also use ``Alt+Left Arrow`` or
``Alt+Right Arrow`` to switch between the different text-mode terminals.

.. _new-kernel:

Upgrading your kernel and related packages
------------------------------------------------------------------------------------

This section explains how to upgrade your kernel and identifies
potential issues related to this upgrade. You can either install one of
the **linux-image-\*** packages provided by Debian, or compile a customized
kernel from source.

Note that a lot of information in this section is based on the
assumption that you will be using one of the modular Debian kernels,
together with **initramfs-tools** and **udev**. If you choose to use a custom
kernel that does not require an initrd or if you use a different initrd
generator, some of the information may not be relevant for you.

.. _kernel-metapackage:

Installing a kernel metapackage
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

When you full-upgrade from |OLDRELEASENAME| to |RELEASENAME|, it is strongly
recommended that you install a linux-image-\* metapackage, if you have
not done so before. These metapackages will automatically pull in a
newer version of the kernel during upgrades. You can verify whether you
have one installed by running:

.. code-block:: console

   $ dpkg -l 'linux-image*' | grep ^ii | grep -i meta
       

If you do not see any output, then you will either need to install a new
linux-image package by hand or install a linux-image metapackage. To see
a list of available linux-image metapackages, run:

.. code-block:: console

   $ apt-cache search linux-image- | grep -i meta | grep -v transition
       

If you are unsure about which package to select, run ``uname -r``
and look for a package with a similar name. For example, if
you see "``4.9.0-8-amd64``", it is recommended that you install
**linux-image-amd64**. You may also use ``apt`` to see a long description of
each package in order to help choose the best one available. For
example:

.. code-block:: console

   $ apt show linux-image-amd64
       

You should then use ``apt install`` to install it. Once this new kernel
is installed you should reboot at the next available opportunity to get
the benefits provided by the new kernel version. However, please have a
look at :ref:`before-first-reboot` before performing the first
reboot after the upgrade.

For the more adventurous there is an easy way to compile your own custom
kernel on Debian. Install the kernel sources, provided in the
**linux-source** package. You can make use of the ``deb-pkg`` target
available in the sources' makefile for building a binary package. More
information can be found in the `Debian Linux Kernel
Handbook <https://kernel-team.pages.debian.net/kernel-handbook/>`__,
which can also be found as the **debian-kernel-handbook** package.

If possible, it is to your advantage to upgrade the kernel package
separately from the main ``full-upgrade`` to reduce the chances of a
temporarily non-bootable system. Note that this should only be done
after the minimal upgrade process described in `Minimal system
upgrade <#minimal-upgrade>`__.

.. _for-next-release:

Preparing for the next release
------------------------------------------------------------

After the upgrade there are several things you can do to prepare for the
next release.

-  Remove newly redundant or obsolete packages as described in `Make
   sure you have sufficient space for the upgrade <#sufficient-space>`__
   and `Obsolete packages <#obsolete>`__. You should review which
   configuration files they use and consider purging the packages to
   remove their configuration files. See also `Purging removed
   packages <#purge-removed-packages>`__.

.. _purge-removed-packages:

Purging removed packages
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

It is generally advisable to purge removed packages. This is especially
true if these have been removed in an earlier release upgrade (e.g. from
the upgrade to |OLDRELEASENAME|) or they were provided by third-party
vendors. In particular, old init.d scripts have been known to cause
issues.

.. caution::

   Purging a package will generally also purge its log files, so you
   might want to back them up first.

The following command displays a list of all removed packages that may
have configuration files left on the system (if any):

.. code-block:: console

   $ apt list '~c'
       

The packages can be removed by using ``apt purge``.
Assuming you want to purge all of them in one go, you can
use the following command:

.. code-block:: console

   # apt purge '~c'
       

.. _obsolete:

Obsolete packages
---------------------------------------------------

Introducing lots of new packages, |RELEASENAME| also retires and omits
quite a few old packages that were in |OLDRELEASENAME|. It provides no
upgrade path for these obsolete packages. While nothing prevents you
from continuing to use an obsolete package where desired, the Debian
project will usually discontinue security support for it a year after
|RELEASENAME|'s release [5]_, and will not normally provide other support
in the meantime. Replacing them with available alternatives, if any, is
recommended.

There are many reasons why packages might have been removed from the
distribution: they are no longer maintained upstream; there is no longer
a Debian Developer interested in maintaining the packages; the
functionality they provide has been superseded by different software (or
a new version); or they are no longer considered suitable for
|RELEASENAME| due to bugs in them. In the latter case, packages might
still be present in the "unstable" distribution.

"Obsolete and Locally Created Packages" can be listed and purged from
the commandline with:

.. code-block:: console

   $ apt list '~o'
   # apt purge '~o'
     

The `Debian Bug Tracking System <https://bugs.debian.org/>`__ often provides additional
information on why the package was removed. You should review both the
archived bug reports for the package itself and the archived bug reports
for the
`ftp.debian.org pseudo-package <https://bugs.debian.org/cgi-bin/pkgreport.cgi?pkg=ftp.debian.org&archive=yes>`__.

For a list of obsolete packages for |RELEASENAME|, please refer to
:ref:`noteworthy-obsolete-packages`.

.. _dummy-packages:

Transitional dummy packages
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Some packages from |OLDRELEASENAME| may have been replaced in |RELEASENAME|
by transitional dummy packages, which are empty placeholders designed to
simplify upgrades. If for instance an application that was formerly a
single package has been split into several, a transitional package may
be provided with the same name as the old package and with appropriate
dependencies to cause the new ones to be installed. After this has
happened the redundant dummy package can be safely removed.

The package descriptions for transitional dummy packages usually
indicate their purpose. However, they are not uniform; in particular,
some "dummy" packages are designed to be kept installed, in order to
pull in a full software suite, or track the current latest version of
some program. You might also find ``deborphan`` with the ``--guess-*``
options (e.g. ``--guess-dummy``) useful to detect transitional dummy
packages on your system.

.. [1]
   If the debconf priority is set to a very high level you might prevent
   configuration prompts, but services that rely on default answers that
   are not applicable to your system will fail to start.

.. [2]
   For example: DNS or DHCP services, especially when there is no
   redundancy or failover. In the DHCP case end-users might be
   disconnected from the network if the lease time is lower than the
   time it takes for the upgrade process to complete.

.. [3]
   This feature can be disabled by adding the parameter ``panic=0`` to
   your boot parameters.

.. [4]
   Debian's package management system normally does not allow a package
   to remove or replace a file owned by another package unless it has
   been defined to replace that package.

.. [5]
   Or for as long as there is not another release in that time frame.
   Typically only two stable releases are supported at any given time.
