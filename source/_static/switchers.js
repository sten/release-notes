(function() {
  'use strict';

  var all_languages = {
      'be': 'Belarusian',
      'ca': 'Catalan',
      'da': 'Danish',
      'de': 'German',
      'el': 'Greek',
      'en': 'English',
      'es': 'Spanish',
      'fi': 'Finnish',
      'fr': 'French',
      'gl': 'Galician',
      'it': 'Italian',
      'ja': 'Japanese',
      'lt': 'Lithuanian',
      'ml': 'Malayalam',
      'nb': 'Norwegian bokmål',
      'nl': 'Dutch',
      'pl': 'Polish',
      'pt': 'Portuguese',
      'pt-br': 'Brazilian Portuguese',
      'ro': 'Romanian',
      'ru': 'Russian',
      'sk': 'Slovak',
      'sv': 'Swedish',
      'vi': 'Vietnamese',
      'zh-cn': 'Simplified Chinese',
      'zh-tw': 'Traditional Chinese',
  };

  function build_language_select(current_language) {
    var buf = ['<select>'];

    $.each(all_languages, function(language, title) {
      if (language == current_language)
        buf.push('<option value="' + language + '" selected="selected">' +
                 all_languages[current_language] + '</option>');
      else
        buf.push('<option value="' + language + '">' + title + '</option>');
    });
    if (!(current_language in all_languages)) {
        // In case we're browsing a language that is not yet in all_languages.
        buf.push('<option value="' + current_language + '" selected="selected">' +
                 current_language + '</option>');
        all_languages[current_language] = current_language;
    }
    buf.push('</select>');
    return buf.join('');
  }

  function navigate_to_first_existing(urls) {
    // Navigate to the first existing URL in urls.
    var url = urls.shift();
    if (urls.length == 0) {
      window.location.href = url;
      return;
    }
    $.ajax({
      url: url,
      success: function() {
        window.location.href = url;
      },
      error: function() {
        navigate_to_first_existing(urls);
      }
    });
  }

  function on_language_switch() {
    var selected_language = $(this).children('option:selected').attr('value') + '/';
    var url = window.location.href;
    var current_language = language_segment_from_url(url);
    if (selected_language == 'en/') // Special 'default' case for english.
      selected_language = '';
    var new_url = url.replace('releasenotes/' + current_language,
                              'releasenotes/' + selected_language);
    if (new_url != url) {
      navigate_to_first_existing([
        new_url,
        'https://www.debian.org/releases/trixie/releasenotes/'
      ]);
    }
  }

  // Returns the path segment of the language as a string, like 'fr/'
  // or '' if not found.
  function language_segment_from_url(url) {
    var language_regexp = 'releasenotes/([a-z]{2}(?:-[a-z]{2})?/)';
    var match = url.match(language_regexp);
    if (match !== null)
        return match[1];
    return '';
  }

  $(document).ready(function() {
    var release = DOCUMENTATION_OPTIONS.VERSION;
    var language_segment = language_segment_from_url(window.location.pathname);
    var current_language = language_segment.replace(/\/+$/g, '') || 'en';
    var language_select = build_language_select(current_language);

    $('.language_switcher_placeholder').html(language_select);
    $('.language_switcher_placeholder select').bind('change', on_language_switch);
  });
})();
